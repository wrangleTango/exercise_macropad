from collections import namedtuple
from collections import deque
import json
from adafruit_macropad import MacroPad


# First, I set the structure of the data and output it to a JSON for use
# between program runs. This will need to be split off into a different module.
#TODO: split JSON generation into a 'first run' module.
"""exercise_grouping = {'back': ['pullup',
          'pulldown',
          'thorassic_i',
          'thorassic_t',
          'thorassic_y',
          'db_bent_row',
          'single_arm_db_row',
          'single_arm_db_incline_row',
          'seated_row'],
 'bicep_and_tricep': ['bicep_curl',
                      'hammer_curl',
                      'tricep_pulldown',
                      'single_db_tricep_press',
                      'tricep_overhead_db_bench_circles'],
 'chest': ['pushup',
           'db_bench_press',
           'db_incline_bench_press',
           'db_decline_bench_press',
           'cable_fly',
           'chin_up'],
 'core': ['dead_bug',
          'russian_twist',
          'v-up_rollup',
          'cable_side_twist',
          'side_plank',
          'kayak_twist',
          'crunch_ups'],
 'legs': ['squat',
          'kb_swing',
          'alternating_lunge',
          'side_lunge',
          'medial_leg_raise_floor',
          'medial_leg_raise_standing',
          'hack_squat',
          'leg_press',
          'leg_extension_machine',
          'hip_abductor_machine',
          'back_extension_rack',
          'romanian_split_lunge',
          'bulgarian_split_lunge',
          'alternating_lunge_walk',
          'wall_sit',
          'deadlift',
          'single_db_deadlift_balance'],
 'shoulders': ['db_overhead_shoulder_press',
               'crossover_overhead_shoulder_press',
               'machine_shoulder_press',
               'db_deltoid_raise',
               'front_lat_raise']}


with open("exercise_json", "w") as f:
    json.dump(exercise_grouping, f, indent=4) 
"""
# During the workout, selecting and recording exercise data needs to be easy.
# The rotary encoder selects from a predefined set of data values.
# Ordering selections reduces the amount UI feedback required.
Exercise_set = namedtuple("Exercise_set", ["group", "name", "sets",
                                           "reps", "weight", "drop_hold_increase"])

with open("exercise_json", "r") as re_file:
    exercise_read_in = json.load(re_file)


def selection_sequence():
    # selection_sequence() uses rotary dial encoding to select from a displayed list
    pass
    
    #TODO display exercise groups on screen and then select
    #TODO display push/pull/lift and select to drill down to exercise listing
    #TODO display most recent info of selected exercise
    #TODO provide option to add new details for a new entry
    #TODO accept numbers for sets, reps, weight and future drop/hold/increase
    #TODO write new entry to file
    