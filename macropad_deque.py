from collections import deque

test_list = ["chest", "back", "legs", "shoulders", "biceps and triceps"]

def display_three_lines(display_circle):
    three_line_output = []
    for count, part in enumerate(display_circle):
        if count < 3:
            three_line_output.append(part)
    return three_line_output
            

last_position = None
triple_list = None

encoder_value = 1

 
def display_scroll(sequence_to_scroll, supplied_position, display_lines=5):
    display_circle = deque(sequence_to_scroll, display_lines)
    if supplied_position == None:
        # circular array is at beginning of list or hasn't been rotated yet.
        last_position = 0 + encoder_value
        return display_circle, last_position
    elif (supplied_position + encoder_value) < supplied_position:
        # encoder has been turned CCW
        display_circle.rotate(1)
        last_position = supplied_position + encoder_value
        return display_circle, last_position
    elif (supplied_position + encoder_value) > supplied_position:
        # encoder has been turned CW
        display_circle.rotate(-1)
        last_position = supplied_position + encoder_value
        return display_circle, last_position
    else:
        # First time through the loop, resolves first assignment of
        # last position from None type.
        display_circle.rotate(-1)
        last_position = 0 + encoder_value
        return display_circle, last_position