from collections import deque


test_list = ["chest", "back", "legs", "shoulders",
             "biceps and triceps"]
encoder_value = 1

def display_three_lines(display_circle):
    three_line_output = []
    for count, part in enumerate(display_circle):
        if count < 3:
            three_line_output.append(part)
    return three_line_output
         
def inter_scroll_cleanup():
    encoder_value = 0

 
def display_scroll(sequence_to_scroll, display_lines=5):
    display_circle = deque(sequence_to_scroll, display_lines)
    if encoder_value == None or encoder_value == 0:
        # circular array is at beginning of list or hasn't been
        #rotated yet.
        return display_circle
    elif encoder_value < 0:
        # encoder has been turned CCW
        display_circle.rotate(1)
        return display_circle
    elif encoder_value > 0:
        # encoder has been turned CW
        display_circle.rotate(-1)
        return display_circle
    
# make sure test runs operate on the deque returned by the function
# rather than the original list.

scroll_out = display_scroll(test_list)
print(display_three_lines(scroll_out))
encoder_value = -1
scroll_out = display_scroll(scroll_out)
print(display_three_lines(scroll_out))

encoder_value = -1
scroll_out = display_scroll(scroll_out)
print(display_three_lines(scroll_out))